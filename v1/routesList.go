package v1

import (
	"fmt"
	"strings"
)

type RouteList struct {
	subPatches   map[string]*RouteList
	routes       map[Method]*Route
	hasParam     bool
	param        *queryParam
	routesByName map[string]*Route
}

func NewRouteList() *RouteList {
	return &RouteList{
		subPatches:   make(map[string]*RouteList),
		routes:       make(map[Method]*Route),
		routesByName: make(map[string]*Route),
	}
}

func (rl *RouteList) AddRoute(route *Route) error {
	fullPath := route.uri
	if route.group != nil {
		fullPath = route.group.uri + fullPath
	}
	fullPath = strings.Trim(fullPath, "/")
	fullPathSliced := strings.Split(fullPath, "/")
	err := rl.addRouteBySlice(fullPathSliced, route)
	if err != nil {
		return err
	}
	if route.name != "" {
		if _, hasName := rl.routesByName[route.name]; hasName {
			return fmt.Errorf("route with name [%s] already exists", route.name)
		}
		rl.routesByName[route.name] = route
	}
	return nil
}

func (rl *RouteList) AddRoutes(routes ...*Route) error {
	for _, route := range routes {
		if err := rl.AddRoute(route); err != nil {
			return err
		}
	}
	return nil
}

func (rl *RouteList) AddGroup(group *Group) error {
	for _, route := range group.routes {
		if err := rl.AddRoute(route); err != nil {
			return err
		}
	}
	return nil
}

func (rl *RouteList) AddGroups(groups ...*Group) error {
	for _, group := range groups {
		if err := rl.AddGroup(group); err != nil {
			return err
		}
	}
	return nil
}

func (rl *RouteList) AddController(path string, controller Controller) error {
	routes := make([]*Route, 4)
	routes = append(
		routes,
		NewRoute(GET, path, controller.Get),
		NewRoute(POST, path, controller.Post),
		NewRoute(PUT, path, controller.Put),
		NewRoute(DELETE, path, controller.Delete),
	)
	return rl.AddRoutes(routes...)
}

func (rl *RouteList) addRouteBySlice(fullPathSliced []string, route *Route) error {
	if len(fullPathSliced) == 0 {
		if _, ok := rl.routes[route.method]; ok {
			return fmt.Errorf("path [%s] already use", route.uri)
		}
		rl.routes[route.method] = route
		return nil
	}

	param, found, err := parseQueryParam(fullPathSliced[0])
	if err != nil {
		return err
	}

	if found {
		if rl.hasParam {
			return rl.param.subPatches.addRouteBySlice(fullPathSliced[1:], route)
		}
		rl.hasParam = true
		rl.param = param
		return rl.param.subPatches.addRouteBySlice(fullPathSliced[1:], route)
	}
	queryPart := strings.ToLower(fullPathSliced[0])
	subPatches, ok := rl.subPatches[queryPart]
	if ok {
		return subPatches.addRouteBySlice(fullPathSliced[1:], route)
	}
	subPatches = NewRouteList()
	err = subPatches.addRouteBySlice(fullPathSliced[1:], route)
	if err != nil {
		return err
	}
	rl.subPatches[queryPart] = subPatches
	return nil
}

func (rl *RouteList) FindRoute(url string, method string) (*Route, map[string]interface{}, error) {
	url = strings.Trim(url, "/ ")
	params := make(map[string]interface{})
	route, err := rl.findRouteBySlice(strings.Split(url, "/"), Method(method), params)

	return route, params, err
}

func (rl *RouteList) findRouteBySlice(url []string, method Method, params map[string]interface{}) (*Route, error) {
	if len(url) == 0 {
		route, ok := rl.routes[method]
		if !ok {
			return nil, NOT_FOUND_ERROR
		}
		return route, nil
	}
	subList, ok := rl.subPatches[strings.ToLower(url[0])]
	if ok {
		return subList.findRouteBySlice(url[1:], method, params)
	}

	if !rl.hasParam {
		return nil, NOT_FOUND_ERROR
	}

	matched := rl.param.paramPattern.Match([]byte(url[0]))
	if !matched {
		return nil, NOT_FOUND_ERROR
	}
	resultVal, err := parseValue(url[0], rl.param.paramType)
	if err != nil {
		return nil, NOT_FOUND_ERROR
	}
	params[rl.param.paramName] = resultVal
	return rl.param.subPatches.findRouteBySlice(url[1:], method, params)
}

func (rl *RouteList) RouteByName(name string) *Route {
	return rl.routesByName[name]
}
