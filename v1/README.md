##Examples V1
###AddRoutes
```go
package main

import router "bitbucket.org/keekee-proj/router/v1"

func main()  {
 rl := router.NewRouteList()
 rl.AddRoutes(
     router.NewRoute(router.GET, "route1", action1),
     router.NewRoute(router.POST, "route2/i:int/b:bool/s:string", action2),
 )
 rl.AddGroups(
     router.NewGroup("/group1").AddRoute(router.NewRoute(router.PUT, "test3", action3)),
     router.NewGroup("/group2").AddRoutes(
                 router.NewRoute(router.PUT, "test4", action4).Name("route4"),
                 router.NewRoute(router.PUT, "test5", test5),
     ),
 )
 rl.AddController("/controller_path", controller)
}
    
```
###Find route
```go
type Server struct {
	routes  *router.RouteList
}

func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request)  {
    route, params, err := s.routes.FindRoute(req.URL.Path, req.Method)
    if err == router.NOT_FOUND_ERROR  {
        res.WriteHeader(404)
        res.Write([]byte(err.Error()))
        return
    }
    if err != nil {
        // custom error handling
    }
    err = route.Run(res, req, params)
    if err != nil {
        res.WriteHeader(400)
        res.Write([]byte(err.Error()))
        return
    }
}
```

###Middlewares
```go
package main

import (
	router "bitbucket.org/keekee-proj/router/v1"
	"net/http"
	"fmt"
)

func main()  {
 rl := router.NewRouteList()
 route := router.NewRoute(router.GET, "/", handle)
 rl.AddGroup(
     router.NewGroup("/admin/userName:string", AdminMiddleware).AddRoute(route),
 )
}


func AdminMiddleware(r *http.Request, params map[string]interface{}) error {
    if params["userName"] != "admin" {
        return fmt.Errorf("User [%s] is not admin", params["userName"])
    }
    return nil
}
```
#Benchmark
```
go test -bench=Rote -cpu=8 -benchmem -v -parallel=8  tests/routes_benchmark_test.go
goos: darwin
goarch: amd64
BenchmarkRote-8           200000             11615 ns/op           58012 B/op         25 allocs/op
PASS
ok      command-line-arguments  5.725s

```