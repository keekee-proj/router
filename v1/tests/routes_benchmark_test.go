package tests

import (
	router "bitbucket.org/keekee-proj/router/v1"
	"fmt"
	"math/rand"
	"net/http"
	"testing"
)

func BenchmarkRote(b *testing.B) {
	rl := router.NewRouteList()
	for i := 0; i < b.N; i++ {
		rl.AddRoute(
			router.NewRoute(router.GET, fmt.Sprintf("test%d/i:int/b:bool/s:string", i), handler),
		)
	}
	b.ResetTimer()
	n := int(float32(b.N) * 1.5)
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			rl.FindRoute(fmt.Sprintf("/test%d/1/on/test", rand.Intn(n)), "GET")
		}
	})
}

func handler(res http.ResponseWriter, req *http.Request, params map[string]interface{}) error {
	return nil
}
