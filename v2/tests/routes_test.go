package tests

import (
	router "bitbucket.org/keekee-proj/router/v2"
	"bytes"
	"context"
	"fmt"
	"net/http"
	"testing"
)

type responseMock struct {
	result string
}

func (r *responseMock) Header() http.Header {
	return make(http.Header)
}
func (r *responseMock) Write(b []byte) (int, error) {
	r.result = string(b)
	return len(b), nil
}
func (r *responseMock) getResult() string {
	return r.result
}
func (r *responseMock) WriteHeader(code int) {
}

func TestRote(t *testing.T) {
	rl := router.NewRouteList()
	rl.AddRoutes(
		router.NewRoute(router.GET, "test1", test1).Name("test1"),
		router.NewRoute(router.POST, "test2/i:int/b:bool/s:string", test2).Name("test2"),
	)
	rl.AddGroup(
		router.NewGroup("/test").AddRoute(router.NewRoute(router.PUT, "test3", test3).Name("test3")),
	)

	t.Run("Test1: simple", func(t *testing.T) {
		t.Run("Ok", func(t *testing.T) {
			req, err := http.NewRequest("GET", "/test1", bytes.NewReader([]byte{}))
			if err != nil {
				t.Fail()
			}
			resp := responseMock{}
			err = rl.Execute(context.Background(), &resp, req)
			if resp.result != "test1" {
				t.Fail()
			}
		})
		t.Run("Fail", func(t *testing.T) {
			_, _, err := rl.FindRoute("/test1", "PUT")
			if err == nil {
				t.Fail()
			}
		})
	})
	t.Run("Test2: params", func(t *testing.T) {
		t.Run("Ok", func(t *testing.T) {
			tRoute, params, err := rl.FindRoute("/test2/1/1/test", "POST")
			if err != nil {
				t.Fail()
			}
			if len(params) != 3 {
				t.Fail()
			}
			if params["i"] != 1 {
				t.Fail()
			}
			if params["b"] != true {
				t.Fail()
			}
			if params["s"] != "test" {
				t.Fail()
			}
			resp := responseMock{}
			tRoute.Run(context.Background(), &resp, nil)
			if resp.result != "test2" {
				t.Fail()
			}

		})
		t.Run("Fail", func(t *testing.T) {
			_, _, err := rl.FindRoute("/test2/s/1/test", "POST")
			if err != router.NotFoundErr {
				t.Fail()
			}
		})
		t.Run("Bool params", func(t *testing.T) {
			valsTrue := []string{"1", "ok", "true"}
			valsFalse := []string{"0", "noNe", "False", "false"}
			for _, valTrue := range valsTrue {
				url := fmt.Sprintf("/test2/1/%s/test", valTrue)
				_, params, _ := rl.FindRoute(url, "POST")
				if params["b"] != true {
					t.Fail()
				}
			}
			for _, valFalse := range valsFalse {
				url := fmt.Sprintf("/test2/1/%s/test", valFalse)
				_, params, _ := rl.FindRoute(url, "POST")
				if params["b"] != false {
					t.Fail()
				}
			}
		})
	})
	t.Run("Test3: groups", func(t *testing.T) {
		t.Run("Ok", func(t *testing.T) {
			tRoute, params, err := rl.FindRoute("/test/test3", "PUT")
			if err != nil {
				t.Fail()
			}
			if len(params) > 1 {
				t.Fail()
			}
			resp := responseMock{}
			tRoute.Run(context.Background(), &resp, nil)
			if resp.result != "test3" {
				t.Fail()
			}
		})
		t.Run("Ok", func(t *testing.T) {
			tRoute, params, err := rl.FindRoute("/test/test3", "PUT")
			if err != nil {
				t.Fail()
			}
			if len(params) > 1 {
				t.Fail()
			}
			resp := responseMock{}
			tRoute.Run(context.Background(), &resp, nil)
			if resp.result != "test3" {
				t.Fail()
			}
		})
	})
	t.Run("Test4: names", func(t *testing.T) {
		t.Run("simple", func(t *testing.T) {
			tRoute := rl.RouteByName("test1")
			if tRoute == nil {
				t.Fail()
			}
			path, err := tRoute.MakeUri(nil)
			if err != nil {
				t.Fail()
			}
			if path != "/test1" {
				t.Fail()
			}
		})
		t.Run("params", func(t *testing.T) {
			tRoute := rl.RouteByName("test2")
			if tRoute == nil {
				t.Fail()
			}
			params := make(map[string]interface{}, 3)
			params["i"] = 321
			params["b"] = true
			params["s"] = "test"
			path, err := tRoute.MakeUri(params)
			if err != nil {
				t.Fail()
			}
			if path != "/test2/321/on/test" {
				t.Fail()
			}
		})
		t.Run("group", func(t *testing.T) {
			tRoute := rl.RouteByName("test3")
			if tRoute == nil {
				t.Fail()
			}
			path, err := tRoute.MakeUri(nil)
			if err != nil {
				t.Fail()
			}
			if path != "/test/test3" {
				t.Fail()
			}
		})
		t.Run("params fail", func(t *testing.T) {
			tRoute := rl.RouteByName("test2")
			if tRoute == nil {
				t.Fail()
			}
			_, err := tRoute.MakeUri(nil)

			if err == nil {
				t.Fail()
			}
			if err.Error() != "not found param [i]" {
				t.Fail()
			}
		})
	})
}

func TestMiddlewares(t *testing.T) {
	route := router.NewRoute(router.GET, "/", test1).Name("adminRoot")
	rl := router.NewRouteList()
	rl.AddGroup(
		router.NewGroup("/admin/userName:string", AdminMiddleware).AddRoute(route),
	)
	t.Run("Test Middlewares:", func(t *testing.T) {
		t.Run("Ok", func(t *testing.T) {
			req, err := http.NewRequest("GET", "/admin/admin", bytes.NewReader([]byte{}))
			if err != nil {
				t.Fail()
			}
			resp := responseMock{}
			err = rl.Execute(context.Background(), &resp, req)
			if err != nil {
				t.Fail()
			}
		})
		t.Run("Fail", func(t *testing.T) {
			req, err := http.NewRequest("GET", "/admin/me", bytes.NewReader([]byte{}))
			if err != nil {
				t.Fail()
			}
			resp := responseMock{}
			err = rl.Execute(context.Background(), &resp, req)
			if err.Error() != "user [me] is not admin" {
				t.Fail()
			}
		})
	})
}

func TestIdenticalPath(t *testing.T) {
	rl := router.NewRouteList()
	rl.AddRoutes(
		router.NewRoute(router.GET, "/test1/method:string", test1),
		router.NewRoute(router.PUT, "/test1/method:string", test2),
		router.NewRoute(router.POST, "/test1/method:string", test3),
	)
	t.Run("Test Identical Path:", func(t *testing.T) {
		for _, method := range []string{"GET", "POST", "PUT"} {
			t.Run(method, func(t *testing.T) {
				path := fmt.Sprintf("/test1/%s", method)
				_, params, err := rl.FindRoute(path, method)
				if err != nil {
					t.Fail()
				}
				paramMethod, ok := params["method"]
				if !ok {
					t.Fail()
				}
				if paramMethod != method {
					t.Fail()
				}
			})
		}
	})
}

func test1(_ context.Context, res http.ResponseWriter, _ *http.Request) error {
	res.Write([]byte("test1"))
	return nil
}

func test2(_ context.Context, res http.ResponseWriter, _ *http.Request) error {
	res.Write([]byte("test2"))
	return nil
}

func test3(_ context.Context, res http.ResponseWriter, _ *http.Request) error {
	res.Write([]byte("test3"))
	return nil
}
func AdminMiddleware(ctx context.Context, _ *http.Request) error {
	if ctx.Value("userName") != "admin" {
		return fmt.Errorf("user [%s] is not admin", ctx.Value("userName").(string))
	}
	return nil
}
