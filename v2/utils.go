package v2

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"unsafe"
)

var falseValuesPattern = regexp.MustCompile(`(?i)(0|no|off|false|none)`)

func parseValue(strValue string, kind reflect.Kind) (interface{}, error) {
	switch kind {
	case reflect.Int:
		result, err := strconv.ParseInt(strValue, 10, 0)
		if err != nil {
			return nil, err
		}
		return int(result), nil
	case reflect.Int8:
		result, err := strconv.ParseInt(strValue, 10, 8)
		if err != nil {
			return nil, err
		}
		return int8(result), nil
	case reflect.Int16:
		result, err := strconv.ParseInt(strValue, 10, 16)
		if err != nil {
			return nil, err
		}
		return int16(result), nil
	case reflect.Int32:
		result, err := strconv.ParseInt(strValue, 10, 32)
		if err != nil {
			return nil, err
		}
		return int32(result), nil
	case reflect.Int64:
		result, err := strconv.ParseInt(strValue, 10, 64)
		if err != nil {
			return nil, err
		}
		return result, nil
	case reflect.Uint:
		result, err := strconv.ParseUint(strValue, 10, 32)
		if err != nil {
			return nil, err
		}
		return uint(result), nil
	case reflect.Uint8:
		result, err := strconv.ParseUint(strValue, 10, 8)
		if err != nil {
			return nil, err
		}
		return uint8(result), nil
	case reflect.Uint16:
		result, err := strconv.ParseUint(strValue, 10, 16)
		if err != nil {
			return nil, err
		}
		return uint16(result), nil
	case reflect.Uint32:
		result, err := strconv.ParseUint(strValue, 10, 32)
		if err != nil {
			return nil, err
		}
		return uint32(result), nil
	case reflect.Uint64:
		result, err := strconv.ParseUint(strValue, 10, 64)
		if err != nil {
			return nil, err
		}
		return result, nil
	case reflect.Float32:
		result, err := strconv.ParseFloat(strValue, 32)
		if err != nil {
			return nil, err
		}
		return float32(result), nil
	case reflect.Float64:
		result, err := strconv.ParseFloat(strValue, 64)
		if err != nil {
			return nil, err
		}
		return result, nil
	case reflect.String:
		return strValue, nil
	case reflect.Bool:
		if len(strValue) == 0 {
			return false, nil
		}
		matched := falseValuesPattern.Match(*(*[]byte)(unsafe.Pointer(&strValue)))
		return !matched, nil
	default:
		return nil, fmt.Errorf("failed parse value [%q]", strValue)
	}
}

func makeStrValue(value interface{}, kind reflect.Kind) (string, error) {
	if kind == reflect.Int ||
		kind == reflect.Int8 ||
		kind == reflect.Int16 ||
		kind == reflect.Int32 ||
		kind == reflect.Int64 ||
		kind == reflect.Uint ||
		kind == reflect.Uint8 ||
		kind == reflect.Uint16 ||
		kind == reflect.Uint32 ||
		kind == reflect.Uint64 {
		return fmt.Sprintf("%d", value), nil
	}
	if kind == reflect.Float32 ||
		kind == reflect.Float64 {
		return fmt.Sprintf("%f", value), nil
	}
	if kind == reflect.String {
		return value.(string), nil
	}
	if kind == reflect.Bool {
		if value.(bool) {
			return "on", nil
		}
		return "off", nil
	}

	return "", fmt.Errorf("undefined type")
}

func parseQueryParam(path string) (param *queryParam, result bool, err error) {
	searchParam := urlParamPattern.FindAllStringSubmatch(path, 1)
	if len(searchParam) == 0 {
		return nil, false, nil
	}
	paramPattern := ".*"
	if searchParam[0][3] != "" {
		paramPattern = searchParam[0][3]
	}
	paramKind := paramTypes[searchParam[0][4]]

	if paramKind == reflect.Invalid {
		return nil, true, fmt.Errorf("undefined kind of [%s]", searchParam[0][4])
	}
	paramRegexp, err := regexp.Compile(paramPattern)
	if err != nil {
		return nil, true, fmt.Errorf("failed parse pattern [%s]", paramPattern)
	}
	param = &queryParam{
		paramPattern: paramRegexp,
		paramType:    paramKind,
		paramName:    searchParam[0][1],
		subPatches:   NewRouteList(),
	}
	result = true
	return
}

func trim(inS, cutset string) string {
	cutsetRs := *(*[]byte)(unsafe.Pointer(&cutset))
	inStr := func(r byte) bool {
		for _, v := range cutsetRs {
			if v == r {
				return true
			}
		}

		return false
	}
	s, e := 0, len(inS)
	for i := 0; i < e; i++ {
		if i+1 == e {
			return ""
		}

		if !inStr(inS[i]) {
			break
		}
		s++
	}
	for i := len(inS) - 1; i > s; i-- {
		if !inStr(inS[i]) {
			break
		}
		e--
	}
	newB := inS[s:e]
	return newB
}