package v2

import (
	"context"
	"errors"
	"net/http"
	"reflect"
	"regexp"
)

var urlParamPattern = regexp.MustCompile(`^(\w+)(:\((.*)\))?:([\w\d]+)$`)

var paramTypes = map[string]reflect.Kind{
	"bool":    reflect.Bool,
	"int":     reflect.Int,
	"int8":    reflect.Int8,
	"int16":   reflect.Int16,
	"int32":   reflect.Int32,
	"int64":   reflect.Int64,
	"uint":    reflect.Uint,
	"uint8":   reflect.Uint8,
	"uint16":  reflect.Uint16,
	"uint32":  reflect.Uint32,
	"uint64":  reflect.Uint64,
	"float32": reflect.Float32,
	"float64": reflect.Float64,
	"string":  reflect.String,
}

type Method string

const (
	POST   Method = "POST"
	GET    Method = "GET"
	PUT    Method = "PUT"
	DELETE Method = "DELETE"
)

var NotFoundErr = errors.New("not found")

type Middleware func(context.Context, *http.Request) error

type Action func(context.Context, http.ResponseWriter, *http.Request) error

type Controller interface {
	Get(context.Context, http.ResponseWriter, *http.Request) error
	Post(context.Context, http.ResponseWriter, *http.Request) error
	Put(context.Context, http.ResponseWriter, *http.Request) error
	Delete(context.Context, http.ResponseWriter, *http.Request) error
}

type queryParam struct {
	paramName    string
	paramPattern *regexp.Regexp
	paramType    reflect.Kind
	subPatches   *RouteList
}
