##Examples V2
###AddRoutes
```go
package main

import router "bitbucket.org/keekee-proj/router/v2"

func main()  {
 rl := router.NewRouteList()
 rl.AddRoutes(
     router.NewRoute(router.GET, "route1", action1),
     router.NewRoute(router.POST, "route2/i:int/b:bool/s:string", action2),
 )
 rl.AddGroups(
     router.NewGroup("/group1").AddRoute(router.NewRoute(router.PUT, "test3", action3)),
     router.NewGroup("/group2").AddRoutes(
                 router.NewRoute(router.PUT, "test4", action4).Name("route4"),
                 router.NewRoute(router.PUT, "test5", test5),
     ),
 )
 rl.AddController("/controller_path", controller)
}
    
```
###Executing
```go
package main
import (
	router "bitbucket.org/keekee-proj/router/v2"
	"net/http"
	"context"
)

type Server struct {
	routes  *router.RouteList
}

func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request)  {
    ctx := context.Background()
    err := s.routes.Execute(ctx, res, req)
    if err == router.NotFoundErr  {
        res.WriteHeader(404)
        res.Write([]byte(err.Error()))
        return
    } else if err != nil {
        res.WriteHeader(400)
        res.Write([]byte(err.Error()))
    }
}
```

###Middlewares
```go
package main

import (
	router "bitbucket.org/keekee-proj/router/v2"
	"net/http"
	"fmt"
	"context"
)

func main()  {
 rl := router.NewRouteList()
 route := router.NewRoute(router.GET, "/", handler)
 rl.AddGroup(
     router.NewGroup("/admin/userName:string", AdminMiddleware).AddRoute(route),
 )
}


func AdminMiddleware(ctx context.Context, _ *http.Request) error {
    if ctx.Value("userName") != "admin" {
        return fmt.Errorf("user [%s] is not admin", ctx.Value("userName"))
    }
    return nil
}
```
#Benchmark
```
go test -bench=Rote -cpu=4 -benchmem -v -parallel=4  --count=5 tests/routes_benchmark_test.go
goos: darwin
goarch: amd64
BenchmarkRote-4          1000000              1419 ns/op             424 B/op          5 allocs/op
BenchmarkRote-4          1000000              2156 ns/op             424 B/op          5 allocs/op
BenchmarkRote-4          2000000              5956 ns/op             424 B/op          5 allocs/op
BenchmarkRote-4          2000000              4674 ns/op             424 B/op          5 allocs/op
BenchmarkRote-4          1000000              1374 ns/op             424 B/op          5 allocs/op
PASS
ok      command-line-arguments  209.786s


```