package v1

import (
	"strings"
)

type Group struct {
	uri         string
	middlewares []Middleware
	routes      []*Route
}

func NewGroup(path string, middlewares ...Middleware) *Group {
	path = "/" + strings.Trim(path, "/ ")
	return &Group{
		uri:         path,
		middlewares: middlewares,
	}
}

func (g *Group) AddRoute(route *Route) *Group {
	route.group = g
	g.routes = append(g.routes, route)
	return g
}

func (g *Group) AddRoutes(routes ...*Route) *Group {
	for _, route := range routes {
		g.AddRoute(route)
	}
	return g
}
