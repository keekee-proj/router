package v2

type Group struct {
	uri         string
	middlewares []Middleware
	routes      []*Route
}

func NewGroup(path string, middlewares ...Middleware) *Group {
	path = "/" + trim(path, "/ ")
	return &Group{
		uri:         path,
		middlewares: middlewares,
	}
}

func (g *Group) AddRoute(route *Route) *Group {
	return g.AddRoutes(route)
}

func (g *Group) AddRoutes(routes ...*Route) *Group {
	for _, route := range routes {
		route.group = g
	}
	g.routes = append(g.routes, routes...)
	return g
}
