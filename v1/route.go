package v1

import (
	"fmt"
	"net/http"
	"strings"
)

type Route struct {
	group  *Group
	action Action
	uri    string
	method Method
	name   string
}

func NewRoute(method Method, path string, action Action) *Route {
	path = "/" + strings.Trim(path, "/ ")
	route := &Route{
		uri:    path,
		method: method,
		action: action,
	}
	return route
}

func (r *Route) Run(res http.ResponseWriter, req *http.Request, params map[string]interface{}) error {
	if r.group != nil && r.group.middlewares != nil {
		for _, middleware := range r.group.middlewares {
			err := middleware(req, params)
			if err != nil {
				return err
			}
		}
	}
	return r.action(res, req, params)
}

func (r *Route) Name(name string) *Route {
	r.name = name
	return r
}

func (r *Route) MakeUri(params map[string]interface{}) (string, error) {
	fullPath := r.uri
	if r.group != nil {
		fullPath = r.group.uri + fullPath
	}
	fullPath = strings.Trim(fullPath, "/")

	slicedPath := strings.Split(fullPath, "/")
	result := make([]string, 0, len(slicedPath))
	for _, partOfParh := range slicedPath {
		param, found, err := parseQueryParam(partOfParh)
		if err != nil {
			return "", err
		}
		if !found {
			result = append(result, partOfParh)
			continue
		}

		value, ok := params[param.paramName]
		if !ok {
			return "", fmt.Errorf("not found param [%s]", param.paramName)
		}
		strValue, err := makeStrValue(value, param.paramType)
		if err != nil {
			return "", err
		}
		result = append(result, strValue)
	}
	return "/" + strings.Join(result, "/"), nil
}
