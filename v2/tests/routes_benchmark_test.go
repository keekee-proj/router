package tests

import (
	router "bitbucket.org/keekee-proj/router/v2"
	"context"
	"fmt"
	"net/http"
	"testing"
)

func BenchmarkRote(b *testing.B) {
	rl := router.NewRouteList()
	routesLst := make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		rl.AddRoute(
			router.NewRoute(router.GET, fmt.Sprintf("test%d/i:int/b:bool/s:string", i), handler),
		)
		routesLst = append(routesLst, fmt.Sprintf("/test%d/%d/on/test", i, i))
	}
	b.ResetTimer()
	i := 0
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			rl.FindRoute(routesLst[i], "GET")
			i++
		}
	})
}

func handler(_ context.Context, _ http.ResponseWriter, _ *http.Request) error {
	return nil
}
